import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { CustomerComponent } from './customer/customer.component';
import { DataService } from './data.service';
import { FilterPipe } from './filter.pipe';

@NgModule({
    declarations: [
        AppComponent,
        CustomerComponent,
        FilterPipe
    ],
    imports: [
        BrowserModule,
        FormsModule
    ],
    bootstrap: [
        AppComponent
    ],
    providers: [
        DataService
    ]
})

export class AppModule{

}