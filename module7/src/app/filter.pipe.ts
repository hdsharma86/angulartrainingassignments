import { Pipe, PipeTransform } from '@angular/core';
import { DataModel } from './data.model';
@Pipe({
    name: 'filter'
})

export class FilterPipe implements PipeTransform{
    transform(value: DataModel[], keyword: string) {
        keyword = keyword ? keyword.toLowerCase() : null;
        return keyword ? value.filter((customer: DataModel) =>
            (customer.name.toLowerCase().indexOf(keyword) !== -1)) : value;
    }
}

