import { Injectable } from '@angular/core';
import { DataModel } from './data.model';

@Injectable()

export class DataService {
    
    items: DataModel[] = [
        {name: 'Pellentesque', phone: '1234567890', email: 'test1@test.com', address:'test address'},
        {name: 'Vivamus', phone: '4568524961', email: 'test2@test.com', address:'test address'},
        {name: 'Imperdiet', phone: '8524679513', email: 'test3@test.com', address:'test address'},
        {name: 'Phasellus', phone: '8524679513', email: 'test4@test.com', address:'test address'},
        {name: 'ullamcorper', phone: '8524679513', email: 'test5@test.com', address:'test address'},
        {name: 'Phasellus', phone: '8524679513', email: 'test6@test.com', address:'test address'}
    ];

    getData(): DataModel[]{

        return this.items;

    }

}