import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerComponent } from './customer.component';
import { FormsModule } from '@angular/forms';
import { FilterPipe } from '../filter.pipe';

describe('CustomerComponent', ()=>{
    let component: CustomerComponent;
    let fixture: ComponentFixture<CustomerComponent>;

    beforeEach(async(()=>{
        TestBed.configureTestingModule({
            imports: [ FormsModule ],
            declarations: [ 
                CustomerComponent,
                FilterPipe
            ],
        }).compileComponents();
    }));

    beforeEach(()=>{
        fixture = TestBed.createComponent(CustomerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', ()=>{
        expect(component).toBeTruthy();
    })
});