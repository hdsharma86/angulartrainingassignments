import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { DataModel } from '../data.model';

@Component({
    selector: 'app-customer',
    templateUrl: './customer.component.html',
    providers: [DataService]
})

export class CustomerComponent{

    constructor( private _dataService: DataService ){}

    keyword: string;
    data: DataModel[];
    title: string = 'Module 7 - Unit Testing';

    ngOnInit(): void{
        this.data =  this._dataService.getData();
    }
    
}