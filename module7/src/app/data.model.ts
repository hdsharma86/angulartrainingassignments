export interface DataModel {
    name: String;
    email: String;
    phone: String;
    address: String
}
