import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AdminComponent } from './admin.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        AdminComponent
    ],
    exports: [
        CommonModule,
        FormsModule,
        AdminComponent
    ]
})

export class AdminModule{
    
}