import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { CompileShallowModuleMetadata } from '@angular/compiler';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html'
})

export class AdminComponent implements OnChanges{

    @Input() keywords: string;
    @Output() filterData: EventEmitter<any[]> = new EventEmitter<any[]>();
    
    filteredData: any[];

    data: any[] = [
        {username: 'Pellentesque', phone: '1234567890', email: 'test1@test.com'},
        {username: 'Vivamus', phone: '4568524961', email: 'test2@test.com'},
        {username: 'Imperdiet', phone: '8524679513', email: 'test3@test.com'},
        {username: 'Phasellus', phone: '8524679513', email: 'test4@test.com'},
        {username: 'ullamcorper', phone: '8524679513', email: 'test5@test.com'},
        {username: 'Phasellus', phone: '8524679513', email: 'test6@test.com'}
    ];

    ngOnChanges(): void {
        if(this.keywords !== undefined){
            this.filteredData =  this.data.filter((item) => {
                return (item.username.toLowerCase().indexOf(this.keywords) !== -1)
            });
        } else {
            this.filteredData = this.data;
        }
    }
}