import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { UserModule } from './user/user.module';
import { AppComponent } from './app.component';
//import { UserComponent } from './user/user.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        UserModule
    ],
    bootstrap: [AppComponent],
    providers: []
})

export class AppModule{}