import { Injectable } from '@angular/core';
import { DataModel } from './data.model';

@Injectable()

export class DataService {

    items: DataModel[] = [
        {
            'name': 'Myexperiments with Truth',
            'author': 'M.K.Gandhi ',
            'price': 145,
            'date': '25 Jan, 1999'
        }, {
            'name': 'The Merchant of venice',
            'author': 'William shakespeare',
            'price': 543,
            'date': '26 Jan, 1999'
        }, {
            'name': 'A Tale of Two Cities',
            'author': 'Charles Dickens ',
            'price': 64,
            'date': '27 Jan, 1999'
        }, {
            'name': 'Origin of species',
            'author': 'charles Darwin ',
            'price': 283,
            'date': '30 Jan, 1999'
        }, {
            'name': 'Time Machine ',
            'author': 'H.G. Wells ',
            'price': 12,
            'date': '25 Mar, 1999'
        }, {
            'name': 'Mein Kampf ',
            'author': 'Adolf Hitler ',
            'price': 384,
            'date': '28 Jan, 1999'
        }, {
            'name': 'Invisible Man  ',
            'author': 'H.G. Wells',
            'price': 123,
            'date': '29 Jan, 1999'
        }
    ];

    getData(): DataModel[] {
        return this.items;
    }
}