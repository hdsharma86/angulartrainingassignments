import { Component } from '@angular/core';
import { DataService } from '../data.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html'
})

export class HomeComponent{
    constructor( private _dataService : DataService ){}

    items = this._dataService.getData();

    
}