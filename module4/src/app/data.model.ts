export interface DataModel{
    name: string,
    author: string,
    price: number,
    date: string
}