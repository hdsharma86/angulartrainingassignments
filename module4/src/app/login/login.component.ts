import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validator, AbstractControl, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginModel } from './login.model';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    login: LoginModel = new LoginModel();

    constructor(private fb: FormBuilder, private _router: Router){}

    ngOnInit(): void{
        this.loginForm = this.fb.group({
            username: ['',[Validators.required, Validators.minLength(3)]],
            password:['',[Validators.required, Validators.minLength(6)]]
        })
    }

    save(form: NgForm): void{
        let username = this.loginForm.value.username;
        let password = this.loginForm.value.password;
        if(username !== '' && password !== ''){
            console.log('Form submitted...');
            this._router.navigate(['/home']);
        }
    }
}