import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { DateFormat } from './dateformat/dateformat.component';
import { FormsModule } from '@angular/forms';
import { DateFormatPipe } from './dateformat/dateformat.pipe';

@NgModule({
    declarations: [
        AppComponent,
        DateFormat,
        DateFormatPipe
    ],
    imports: [
        BrowserModule,
        FormsModule
    ],
    bootstrap: [
        AppComponent
    ],
    providers: []   
})

export class AppModule{}