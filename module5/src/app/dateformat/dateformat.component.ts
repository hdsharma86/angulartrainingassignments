import { Component } from '@angular/core';
import * as moment from 'moment';

@Component({
    selector: 'date-format',
    templateUrl: './dateformat.component.html'
})

export class DateFormat {

    originalDate:string = '20191010';
    myMoment: string = moment( this.originalDate, "YYYYMMDD" ).fromNow();

}