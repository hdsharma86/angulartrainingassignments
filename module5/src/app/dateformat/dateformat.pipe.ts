import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'dateformat'
})

export class DateFormatPipe implements PipeTransform {

    transform( dateinput: string ){
       return moment( dateinput, "YYYYMMDD" ).fromNow();
    }

}