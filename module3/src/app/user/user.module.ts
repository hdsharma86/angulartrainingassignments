import { NgModule } from '@angular/core';
import { UserComponent } from './user.component';
import { AdminModule } from '../admin/admin.module';

@NgModule({
    declarations: [
        UserComponent
    ],
    imports: [
        AdminModule
    ],
    exports: [
        UserComponent
    ]
})

export class UserModule{

}