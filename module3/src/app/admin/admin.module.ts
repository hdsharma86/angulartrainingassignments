import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AdminComponent } from './admin.component';
import { DataService } from '../data.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        AdminComponent
    ],
    exports: [
        CommonModule,
        FormsModule,
        AdminComponent
    ],
    providers: [DataService]
})

export class AdminModule{
    
}