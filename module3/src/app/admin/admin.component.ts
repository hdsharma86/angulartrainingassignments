import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from '../data.service';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html'
})

export class AdminComponent implements OnChanges{

    constructor( private _dataService: DataService ){}

    @Input() keywords: string;
    @Output() filterData: EventEmitter<any[]> = new EventEmitter<any[]>();
    
    filteredData: any[];

    ngOnChanges(): void {
        this.filteredData = this._dataService.getData(this.keywords);
    }
}