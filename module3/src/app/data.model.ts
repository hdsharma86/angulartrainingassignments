export interface DataModel{
    username: string,
    phone: string,
    email: string
}