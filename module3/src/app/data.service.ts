import { Injectable } from '@angular/core';
import { DataModel } from './data.model';

@Injectable()

export class DataService{

    items: DataModel[] = [
        {username: 'Pellentesque', phone: '1234567890', email: 'test1@test.com'},
        {username: 'Vivamus', phone: '4568524961', email: 'test2@test.com'},
        {username: 'Imperdiet', phone: '8524679513', email: 'test3@test.com'},
        {username: 'Phasellus', phone: '8524679513', email: 'test4@test.com'},
        {username: 'ullamcorper', phone: '8524679513', email: 'test5@test.com'},
        {username: 'Phasellus', phone: '8524679513', email: 'test6@test.com'}
    ];

    getData( keyword: string ): DataModel[]{
        if(keyword !== undefined){
            var keyw = keyword.toLowerCase();
            var itms = this.items.filter((item) => {
                return (item.username.toLowerCase().indexOf(keyw) !== -1)
            });
            return itms;
        }

        return this.items;

    }

}