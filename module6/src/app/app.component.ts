import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-main',
    templateUrl: './app.component.html'
})

export class AppComponent{
    
    key: string = 'selected_language'; 
    language: string = localStorage.getItem(this.key);
    constructor( private _translateService: TranslateService ){
        if(this.language){
            this._translateService.use(this.language);
        } else {
            this._translateService.setDefaultLang('en');
        }
        
    }

    title: string = 'Angular Internationalization';
    currentDate = new Date();
    languages: any[] = [
        {code:'en', text: 'English'},
        {code:'fr', text: 'French'}
    ];

    switchLanguage( language: string ){
        console.log(language);
        if(language !== ''){
            localStorage.setItem(this.key, language);
        } else {
            localStorage.setItem(this.key, 'en');
        }
        window.location.reload();
    }

}
