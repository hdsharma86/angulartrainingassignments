import { NgModule, LOCALE_ID }  from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { MultiLang } from './multilang/multilang.component';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeEn from '@angular/common/locales/en';

let currentLanguage = localStorage.getItem('selected_language');
let lang = 'en-US';
if(currentLanguage !== ''){
    lang = (currentLanguage == 'fr') ? 'fr' : 'en-US';
    if(lang == 'fr'){
        registerLocaleData(localeFr, lang);
    } else {
        registerLocaleData(localeEn, lang);
    }
}

export function translateHttpLoaderFactory( http: HttpClient ){
    return new TranslateHttpLoader(http);
}

@NgModule({
    declarations: [
        AppComponent,
        MultiLang
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: translateHttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    bootstrap: [
        AppComponent
    ],
    providers: [
        { provide: LOCALE_ID, useValue: lang }
    ]
})

export class AppModule {
    
}