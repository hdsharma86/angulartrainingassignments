import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-lang',
    templateUrl: './multilang.component.html'
})

export class MultiLang{

    constructor( private _translateService: TranslateService ){
        this._translateService.setDefaultLang('en');
    }

    title: string = 'Angular Internationalization';
    languages: any[] = [
        {code:'en', text: 'English'},
        {code:'fr', text: 'French'}
    ];

    switchLanguage( language: string ){
        console.log(language);
        this._translateService.use(language);
    }
}